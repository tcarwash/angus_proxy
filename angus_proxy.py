from markdown import markdown

def remove_header(response, header_name):
    if header_name in response.headers:
        del response.headers[header_name]


def response(flow):
    # remove security headers in case they're present
    remove_header(flow.response, "Content-Security-Policy")
    remove_header(flow.response, "Strict-Transport-Security")
    # if content-type type isn't available, ignore
    if "content-type" not in flow.response.headers:
        return
    if "text/markdown" in flow.response.headers["content-type"] and flow.response.status_code == 200:
        flow.response.content = bytes(markdown(flow.response.content.decode("utf-8")), "utf-8")
        flow.response.headers["content-type"] = "text/html"
        return
