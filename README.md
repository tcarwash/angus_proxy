# Angus\_Proxy

Making the packet web a little smaller

## What is angus proxy?
Angus\_Proxy is part of an attempt to reduce transmitted data when downloading pages on the [Angusnet](https://github.com/kg7oem/AngusNet) packet radio network. 

## How does it work?
Angus\_Proxy uses `mitmproxy` and a custom configuration script to convert markdown to HTML. Thats it!

HTTP servers serve markdown formatted text files instead of HTML and less data gets sent over the air, less data on the air means fewer packet collisions and fewer retries. 

Potential Benefits:
- Angus\_Proxy doesn't require any modification of the server side, just write pages in markdown with .md file extensions. 
- Markdown files are human readable enough to work in place of HTML if client side doesn't have Angus\_Proxy installed/running.
- Save *dozens* of bits in transmitted data. At 1200 baud it all adds up. 856b of HTML ends up as 730b when transmitted as markdown.
- Browser agnostic. Use lynx, firefox... *Internet Explorer* anything with proxy settings

## Running Angus\_Proxy

1. clone this repository
2. install mitmproxy:

    `sudo apt install mitmproxy` *or similar depending on OS*

    *the version of mitmproxy in the raspbian repository may be out of date, I had much better luck installing with pip*

    `pip3 install mitmproxy`

3. install markdown:

    `pip3 install markdown`

4. run mitmproxy with `angus_proxy.py` as a config file:
    
    `mitmproxy -s angus_proxy.py`

5. Point your browser of choice to `localhost:8080` in it's proxy settings

*Angus\_Proxy will man-in-the-middle all http requests/responses and convert any markdown files it receives to HTML*

## Next Steps

This is a very very first attempt at this, there's no error handling and not really much going on here. *This is a proof of concept*

Future Features:
- Error handling
- More robust handling of responses
    - Maybe don't MITM everything
    - Maybe don't convert markdown that's not from Angusnet hosts
